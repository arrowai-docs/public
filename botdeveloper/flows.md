# Flows

## Create New Flow

* Go to URL here.
* First 'Sign Up', if you are already registered then 'Sign In'.
* Click 'Bots' menu and select your ArrowAI bot.
* After that you will be able to create your first flow by clicking on 'New Flow'.


![Intent list](img/flowlist.png)


* Here you can define your flow name under 'Flow Name' section and add different modules from left side by drag and drop process. i.e., you have to simply drag and drop the module/s(which you want) from left side to the right side below flow section.

![Intent list](img/flowdetail.png)

* You may create different flows as much as you want in your bot.


***


## Modules Description

Module Description includes all the elements which comes under a flow and from here you will be able to know how to use them. So let's get started:

### NLP Intent 
This module is pre-defined module which is created at the creation of the **'flow'**. Here we can do two things:

* We may simply include the NLP which has been created in the NLP section.
* Or, you may directly add the utterances in the flow.

1. Click on the **'NLP Intent'**.
2. There you will be able to see the **'Utterances'** click on that and select 'Match with Similar Sentences' for simple sentences and for regular expressions select 'Regex - Match Sentence Format'.
3. At last, add your utterances in it.

![NLP Intent](img/nlpintent.png)

* In NLP Intent we can include **'Entities'** also to use that in the particular flow. Let see how it can be done:

1. Click on the **'NLP Intent'**.
2. There you will be able to the **'Entities'** click on that.
3. At last, include your entity in it.

![NLP Intent](img/nlpentity.png)


---

### Message

Message is a text module which is used for simple text format. For that you have to simply drag and drop the **'Message'** module under NLP Intent then assign the text which you want.

![NLP Intent](img/messages.png)

```js
Code:
{
   "response": {
      "type": "text",
      "text": "Hey welcome to our testing bot creation.",
      "suggestion": []
   },
   "callAgain": true,
   "variable": {}
}
```



---



### Suggestions 

Use suggestions in form of buttons to hint at responses to continue.

* Under a flow drag and drop the **'Suggestions'** module.
* Then add text and buttons what you want.
* Use **'Add More'** button as you want.

![NLP Intent](img/suggestions.png)



Buttons are of 2 types:

1. **Postback:** These buttons are used to hint at responses to continue like suggestions. It includes: 
    * *Title* - Button title.
    * *Variable Name* - Button variable (this should be same for all the buttons).
    * *Variable Value* - Button's variable value.
    * *Parse Value* - Value that is parsed

![NLP Intent](img/postback.png)

!> **'x'** icon is used to to delete the button module. For more button display you may add buttons in it by clicking on **'+ Add More'**

**Code:**
```js
{
   "response": {
      "type": "button_bottom_template",
      "text": <text_message>,
      "buttons": [
         {
            "loopVariable": "",
            "maxCount": "",
            "type": "postback",
            "url": "",
            "title": <button_title>,
            "variableName": "",
            "variableValue": "",
            "parserAutomatically": true,
            "textParser": "",
            "parserEntity": "",
            "parserArray": [

            ]
         }
      ],
      "suggestion": []
   },
   "callAgain": <boolean_value>,
   "variable": {
      "responseObj": [],
      "system_check_only_postback": true
   }
}
```


| Title | Type | Description |
| --- | --- | --- |
| < text_message > | String | The text which you will send on the chat. |
| < button_title > | String | The text displayed on the button. This text should indicate the purpose of the button. |
| < boolean_value > | Boolean | The flow has to be exited or not. It should be boolean type. |


2. **Web Url:** Web Url is used when we want to link to sites outside the developer's domain are allowed. It includes:
    * Title - Button title.
    * Button Url
    * Repeat Un-Replied Button

![NLP Intent](img/weburl.png)


!> **'x'** icon is used to to delete the button module. For more button display you may add buttons in it by clicking on **'+ Add More'**.

**Code:**
```js
{
   "response": {
      "type": "button_bottom_template",
      "text": <text_message>,
      "buttons": [
         {
            "loopVariable": "",
            "maxCount":"",
            "type": <button_title>,
            "url": <button_url>,
            "title": "Test",
            "variableName": "",
            "variableValue": "",
            "parserAutomatically": true,
            "textParser": "",
            "parserEntity": "",
            "parserArray": [

            ]
         }
      ],
      "suggestion": []
   },
   "callAgain": <boolean_value>,
   "variable": {
      "responseObj": [],
      "system_check_only_postback": true
   }
}
```


| Title | Type | Description |
| --- | --- | --- |
| < text_message > | String | The text which you will send on the chat. |
| < button_title > | String | The text displayed on the button. This text should indicate the purpose of the button. |
| < button_url > | URL | Link of button which you want to be directly directed to that link. |
| < boolean_value > | Boolean | The flow has to be exited or not. It should be boolean type. |

---

### Card 

A card displays information that can include the following:
* Title
* Image
* Description

Use cards mainly for display purposes. They are designed to be concise, to present key (or summary) information to users, and to allow users to learn more if you choose.

In most situations, you should add suggestion below the cards.

![NLP Intent](img/card.png)


* You should add suggestions by clicking on the 'Add Button'.
* For more card display response you may add cards in it by clicking on '+ Add More'.


Buttons are of 2 types:
* **Postback:** These buttons are used to hint at responses to continue like suggestions. It includes: 
    * *Title* - Button title.
    * *Variable Name* - Button variable (this should be same for all the buttons).
    * *Variable Value* - Button's variable value.
    * *Parse Value*
    * *Repeat Un-Replied Button*

* **Web Url:** Web Url is used when we want to link to sites outside the developer's domain are allowed. It includes:
    * *Title* - Button title.
    * *Button Url*
    * *Repeat Un-Replied Button*

!> Hint:  'x' icon is used to to delete the button module.
For more button display you may add buttons in it by clicking on **'+ Add More'**.

**Code:**
```js
{
    "response": {
       "type": "search_result_list",
       "searchResult": [
          {
             "type": "card_list",
             "text": "Men's Fashion",
             "dynamic": false,
             "id": "",
             "loopVariable": "",
             "maxCount": "",
             "title": "Men's Fashion",
             "image": "http://pages.arrowai.com/inorbit/men.jpeg",
             "url": "http://pages.arrowai.com/inorbit/men.jpeg",
             "default_action": {
                "type": "web_url",
                "url": "http://pages.arrowai.com/inorbit/men.jpeg",
                "webview_height_ratio": "tall"
             },
             "buttons": [
                {
                   "type": "postback",
                   "title": "Men's Fashion",
                   "variableName": "Men's Fashion",
                   "payload": {
                      "variable": "location_name",
                      "value": "Men's Fashion",
                      "text": "Men's Fashion"
                   },
                   "variableType": {
                      "type": "regexParser",
                      "valueName": "location_name_Men's Fashion",
                      "values": [
                         "Men's Fashion"
                      ]
                   }
                }
             ]
          }
       ],
       "suggestion": []
    },
    "callAgain": false,
    "variable": {}
}
```

| Title | Description |
| --- | --- |
| Title | Title is a plain text and fixed font and size. |
| Image | Image source is a URL. |
| Description | Title is a plain text and fixed font and size. |
| Default Url | This Url is optional. |



---

### Change Flow
Change flow used to change the flows according to the responses.

**Code:**
```js

```

### Image

When you want to send a response in the image display form then you may use Image module. For that you have to simply drag and drop the **'Image'** module under your flow and add their credentials.

![NLP Intent](img/imagecomp.png)

As you can see in the image that **'Image'** module is of 3 types:
* **Url (default)** It includes:
    * *Image Caption* - The text which will be displayed above the image.
    * *Image Url* - Image which is to be displayed as a response format. It should be JPEG, JPG, or PNG file.
    ![NLP Intent](img/imageurl.png)
* **Variable:** It includes:
* **Short Code:** It includes:

**Code:**
```js
{
   "response": {
      "type": "image_template",
      "images": {
         "loopVariable": "",
         "maxCount": "",
         "type": "url",
         "url": <image_url>,
         "title": <image_title>,
         "short_code": "",
         "order": ""
      },
      "suggestion": []
   },
   "callAgain": false,
   "variable": {}
}
```

| Title | Type | Description |
| --- | --- | --- |
| < image_url > | URL | Link of image which you want to be displayed. It should be JPEG, JPG, or PNG file. |
| < image_title > | String | The title which will be displayed above the image. |


---


### Video 

When you want to set a response in the video display form then you may use Video module. For that you have to simply drag and drop the **'Video'** module under your flow and add their credentials.

![NLP Intent](img/videourl.png)

As you can see in the image that **'Video'** module is of 3 types:

* **Url (default):** It includes:
    * *Video Title* - The text which will be displayed below the video.
    * *Video Url* - Video which is to be played as a response format. It should be MP4 file.

![NLP Intent](img/videou.png)
* **Variable:** It includes:
* **Short Code:** It includes:

**Code:**
```js
{
   "response": {
      "type": "video",
      "video": {
         "loopVariable": "",
         "maxCount": "",
         "type": "url",
         "url": <video_url>,
         "title": <video_title>,
         "short_code": "",
         "order": ""
      },
      "suggestion": []
   },
   "callAgain": false,
   "variable": {}
}
```

| Title | Type | Description |
| --- | --- | --- |
| < video_url > | URL | Link of video which you want to be played. It should be mp4 file. |
| < video_title > | String | The title which will be displayed below the video. |


---

### Audio

When you want to set a response in the audio format then you may use Audio module. For that you have to simply drag and drop the **'Audio'** module under your flow and add their credentials.

![NLP Intent](img/audio.png)

As you can see in the image that **'Video'** module is of 2 types:
* **Url (default):** It includes:
    * *Audio Caption* - The text which will be displayed above the audio.
    * *Audio Url* - Audio which is to be played as a response format. It should be MP3 file.

![NLP Intent](img/audiourl.png)
* **Variable:** It includes:

**Code:**
```js
{
   "response": {
      "type": "audio",
      "text": <audio_text>,
      "audio": {
         "loopVariable": "",
         "maxCount": "",
         "type": "url",
         "url": <audio_url>,
         "short_code": "",
         "order": ""
      },
      "suggestion": []
   },
   "callAgain": false,
   "variable": {}
}
```

| Title | Type | Description |
| --- | --- | --- |
| < audio_url > | URL | Link of audio which you want to be played. It should be mp3 file. |
| < audio_text > | String | The title which will be displayed above the audio. |