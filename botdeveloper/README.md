# ArrowAI Bot Platform

With ArrowAI Bot Platform, you can create your own bot/s by defining different actions and in that you can use the following types of modules:

* **Flows:** 

A flow is a section where you can add your multiple modules in it according to your responses (Like: text message, text message with buttons, cards, media responses, and so on). For more information click here.

* **NLP:** 

A NLP section is for your 'Utterances' where you can train your word/s or letter/s. For more information click here.

* **Entities:** 

* **Functions:** 

* **Kb:** 

* **API:** 

* **Test:** 