# Functions

## Create your function

* Go to URL here.
* Click **'Bots'** menu and select your ArrowAI bot.
* After that you will be able to create your functions under **'Functions'** section.
* Now you have to select your fulfillment type it should be either **'Webhook Url'** or **'In-buit Functions'**.

### Webhook Url: 

Under fulfillment type you have to select the check box of webhook url and paste your url.
![NLP Intent](img/functions.png)

### In-built Functions: