# Building your First ArrowAI Bot

## Create ArrowAI Bot

* Go to URL [here](https://cloudarrowai.web.app).
* First 'Sign Up', if you are already registered then 'Sign In'.
* Click 'Bots' menu and then on 'Create New' to create your new ArrowAI bot.

![logo](img/botlist.png)

* Now click 'Arrow Ai V1' then enter your 'Bot Name' and 'Bot Description' and at last click on 'Save' button to create your ArrowAI Bot

![logo](img/createbot.png)

* Here, you will able to see that your bot has been created. Now click on that to add modules and set response in that.