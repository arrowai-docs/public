# Natural Language Processing

## Create NLP

* Go to URL here.
* Click **'Bots'** menu and select your ArrowAI bot.
* After that you will be able to create your first NLP by clicking on **'+ New NLP'** under NLP section.
![NLP Intent](img/nlp.png)

* Here you can define your nlp name under 'Enter NLP Name' and add your Utterances under Training phrases as much as you want.
![NLP Intent](img/nlpname.png)