# Entities

## Create Entity

* Go to URL here.
* Click 'Bots' menu and select your ArrowAI bot.
* After that you will be able to create your first Entity by clicking on ' + New Entity' under Entities section.
![NLP Intent](img/entitylist.png)
* Here you can define your entity name under 'Entity' and click 'Add Keyword' to add entities.
![NLP Intent](img/addentity.png)
* Now you can add keywords and also their synonyms by clicking on 'Add Keyword'. By clicking on the 'delete icon' you may delete them also.
![NLP Intent](img/entitykeyword.png)