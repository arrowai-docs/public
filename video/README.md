# Video Integration

* First you have to install the npm package @arrowai/video.  

    Just run the below command in the terminal-
         
      npm install @arrowai/video

* Then import this in the component.ts where you want to integrate video as below-

      import { connect } from '@arrowai/video';

* In html file add below code where you want inject video

      <div class="column">                      
       <video id="localVideo" autoplay></video>
      </div>
      <div class="column">
       <video id="remote-media-div" autoplay></video>
      </div>

* In component.ts declare variable “localVideo” and “remote-media-div” (note-it is totally depends on you what variable you want to declare) 

* Add ngAfterViewInit() method, along with this import AfterViewInit. And implements this with class as follow-:
  
      import { AfterViewInit } from '@angular/core';

      export class Component implements AfterViewInit

      ngAfterViewInit() {
       this.localVideoComponent = document.getElementById('localVideo');
       this.remoteMedia = document.getElementById('remote-media-div');
       this.loadArrowaiVideo();
      }

* Then add addMediaStream function-

      attachMediaStream = function (element, stream)
       {
         this.remoteMedia.srcObject = stream;
       };

* Now we have to add laodArrowaiVideo method which we defined in ngAfterViewInit().

      async loadArrowaiVideo()
       {
         let room: any = await connect(room_id, {});
         room.on('localTrackReady', track => {
         this.localVideoComponent.srcObject = track;
         })
  
         room.on('participantConnected', participant => {
         console.log(`Participant "${participant}" connected`);
         console.log(participant);
         const remote_mediaElement = document.createElement('video');
         console.log(participant.stream);
         this.attachMediaStream(remote_mediaElement, participant.stream);
        });
       }

*  In this method we declare a room variable to connect with the room that we create.

   The track  variable gives response of room and in participant variable, there is information of newly added members in video.

* To create a room dynamically use this-

      Api - https://videosignal.arrowai.in/api/room

      Json - 
         {
	      "room":
            {
           	  "applicationId":your_application_id,
           	  "name":room_name,
           	  "type":"p2p"
           }
         }

     Response-
        {
         "status": 1,
         "message": "Room Created Successfully.",
         "data": {
                  "_id": room_id,
                  "applicationId": your_application_id,
                  "name": room_name
                 }
        }

   In response, _id is your roomid that you pass in connect, At the time of calling this api, change applicationId with yours and the room name whatever you want.   
    

